exports.defineAutoTests = function() {
  var registrationURI = "http://localhost:4000/registerStream";
  var publicationURI = "http://localhost:4000/feedStream";
  var templateURI = "http://mw-ysedera.rhcloud.com/template";

  describe('MobileWave Initialization Tests', function() {
    it('Invalid MobileWave initialization - missing registration URI ', function() {
      var options = {};
      expect(function() {
        MobileWave.init(options);
      }).toThrow("Registration URI is not defined!");
    });
    it('Invalid MobileWave initialization - missing publication URI ', function() {
      var options = {
        "registrationURI": registrationURI
      };
      expect(function() {
        MobileWave.init(options);
      }).toThrow("Publication URI is not defined!");
    });
    it('Invalid MobileWave initialization - missing template/templateURI', function() {
      var options = {
        "registrationURI": registrationURI,
        "publicationURI": publicationURI
      };
      expect(function() {
        MobileWave.init(options);
      }).toThrow("No template source was provided, either define 'template' or 'templateURI'!");
    });
    it('Valid MobileWave initialization - local template', function() {
      var options = {
        "template": {
          "context": {
            "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
            "xsd": "http://www.w3.org/2001/XMLSchema#",
            "ssn": "http://purl.oclc.org/NET/ssnx/ssn#",
            "mw": "http://www.semanticweb.org/mobile-wave#",
            "sao": "http://iot.ee.surrey.ac.uk/citypulse/ontologies/sao/sao#",
            "prov": "https://www.w3.org/ns/prov#",
            "generatedAt": {
              "@id": "prov:generated",
              "@type": "http://www.w3.org/2001/XMLSchema#date"
            },
          },
          "dGraph": [{
            "mapping": "mw:SmartPhone",
            "source": "device",
            "target": "SmartPhone_{{uuid}}"
          }, ],
          "iGraph": [{
            "mapping": "mw:GPSSensorOutput",
            "source": "device",
            "target": "GPSSensorOutput_{{uuid}}_{{@ts}}",
            "properties": [{
              "mapping": "ssn:isProducedBy",
              "source": "device",
              "target": "GPSSensor_{{uuid}}"
            }]
          }]

        },
        "registrationURI": registrationURI,
        "publicationURI": publicationURI
      };
      expect(function() {
        MobileWave.init(options);
      }).not.toThrow();
    });
    it('Valid MobileWave initialization - remote template', function() {
      var options = {
        "templateURI": templateURI,
        "registrationURI": registrationURI,
        "publicationURI": publicationURI
      };
      expect(function() {
        MobileWave.init(options);
      }).not.toThrow();
    });
  });
  describe('MobileWave Weaving Tests', function() {
    it('Generate valid jsonLD from template', function() {
      var template = {
        "mapping": "mw:GPSSensorOutput",
        "source": "device",
        "target": "GPSSensorOutput_{{uuid}}_{{@ts}}",
        "properties": [{
          "mapping": "ssn:isProducedBy",
          "source": "device",
          "target": "GPSSensor_{{uuid}}"
        }]
      };

      var data = {
        "uuid": 123456789,
        "@ts": 987654321
      };
      var jsonld = MobileWave._weave(template, data, false);
      console.log(jsonld);
      expect(jsonld["@id"]).toEqual("GPSSensorOutput_123456789_987654321");
      expect(jsonld["@type"]).toEqual("mw:GPSSensorOutput");
      expect(jsonld["ssn:isProducedBy"]["@id"]).toEqual("GPSSensor_123456789");
    });
  });
};
