# MobileWave Cordova Plugin 
MobileWave is a Apache Cordova plug-in that enable ontology based access to mobile sensors.


## Flow
![mw - Page 1.png](https://bitbucket.org/repo/644MAB5/images/2025431874-mw%20-%20Page%201.png)

## Usage
### Prerequisite
1- Download and install [Node.js](https://nodejs.org/en/download/)

2- Download and install [Git Client](https://git-scm.com/downloads)

3- Install Cordova using `npm`
```bash
# Use sudo on mac and linux
npm install -g cordova@6.1.0 
```
### Setup

4- Create a cordova project and add platforms 
```bash
cordova create <project-path> [<project-id>[ <project-name>]]
cordova platform add browser
cordova platform add android
...
```
See more about creating a project [here](https://cordova.apache.org/docs/en/latest/reference/cordova-cli/index.html#cordova-create-command)

5- Go to the project folder `cd <project-path>` and add cordova-plugin-mobile-wave
```bash
cordova plugin add https://bitbucket.org/polimmobilewave/cordova-plugin-mobile-wave.git
```
### Implementation

6- Enable access to external url source, (MobilWave middleware, and template server )

6.1- Open index.html and replace the following line 
```html
<meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-eval'; style-src 'self' 'unsafe-inline'; media-src *; img-src 'self' data: content:;">
```
with this line
```html
<meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-inline';style-src 'self' 'unsafe-inline';media-src *;img-src 'self' data: content:;connect-src *;">
```
see more on [this link](https://content-security-policy.com/)

7- Open <project-path>/www/js/index.js
```javascript
   ...
   onDeviceReady: function() {
    var template = {
      "context": { ... },
      "dGraph": [ ... ],
      "iGraph": [ ... ]
    };

    var options = {
      "template": template,
      "registrationURI": "<registration-uri>",
      "publicationURI": "<publication-uri>",
    };  
    var data = ...; // get the needed data from somewhere
    MobileWave.init(options);
    MobileWave.publish(data, function(msg) {
        console.log(msg);
    });
    ...
```

8- Run Cordova project
```bash
cordova run browser
```

## Parameters
| Parameter        | Explaination |
| -----------------|:-------------|
| `template`       | holds template object to be used to construct JsonLD object  |
| `templateURI`    | URI to download the template if not provided locally. ignore if `template` is set   |
| `registrationURI`| registration endpoint |
| `publicationURI` | publication endpoint |

## Methods
| Method        | Explaination |
| -----------------|:-------------|
| `constructor` | Download template if needed, and register device info using `dGraph` mappings to `registrationURI` |
| `publish`    | publish observation to `publicationURI`|


### Extending the plugin
1- Install plugman
```bash
# Use sudo on mac and linux
npm install -g plugman
```
2- Create a new plugin project 
```bash
plugman create --name <plugin-name> --plugin_id <plugin-id> --plugin_version <plugin-version>
```
3- open plugin.xml, and add the follwing
```xml
<?xml version='1.0' encoding='utf-8'?>
<plugin id="plugin-id"
        version="0.0.1"
        xmlns="http://apache.org/cordova/ns/plugins/1.0"
        xmlns:android="http://schemas.android.com/apk/res/android">
    ...
    <dependency id="cordova-plugin-mobile-wave" url="https://bitbucket.org/polimmobilewave/cordova-plugin-mobile-wave.git"></dependency>
</plugin>
```
4- To add it to a Cordova application, got to project folder and execute 
```bash
cordova plugin add --link <plugin-path>
```

5- Add a new function
```javascript
MobileWave.someFunction = function(){
   ...
};
```

6- That's it.