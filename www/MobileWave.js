(function(root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([], factory);
  } else {
    // Browser globals
    root.MobileWave = factory();
  }
}(this, function() {
  'use strict';
  // Factory Function

  var mw = {};
  var _context = {};
  var _dGraph = {};
  var _iGraph = {};
  var _device = {};

  var _registrationURI = null;
  var _publicationURI = {};

  const TEMPLATE_AT_TS = "timestamp";
  const TEMPLATE_AT_ID = "@id";
  const TEMPLATE_AT_TYPE = "@type";
  const TEMPLATE_AT_CONTEXT = "@context";
  const TEMPLATE_AT_GRAPH = "@graph";

  const TEMPLATE_MAPPING = "mapping";
  const TEMPLATE_TARGET = "target";
  const TEMPLATE_FIELDS = "fields";
  const TEMPLATE_PROPERTIES = "properties";

  const TEMPLATE_SUBJECT = "subject";
  const TEMPLATE_PREDICATE = "predicate";
  const TEMPLATE_OBJECT = "object";
  const TEMPLATE_IS_LITERAL = "isLiteral";

  const TEMPLATE_GENERATED = "generatedAt";
  /*************/
  /* Utilities */
  /*************/
  var request = function(method, url, parameter, doneCB, errorCB) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url, true);

    xhr.onload = function(e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          if (doneCB) {
            doneCB(xhr.responseText, parameter);
          }
        } else {
          if (errorCB) {
            errorCB();
          }
        }
      }
    };
    xhr.send(parameter);
  }

  mw._weave = function(pattern, jObject, isProperty) {
    var obj = {};
    var subject = Mustache.render(pattern[TEMPLATE_SUBJECT], jObject);
    obj[TEMPLATE_AT_ID] = subject;
    var properties = pattern[TEMPLATE_PROPERTIES];
    properties.forEach(function(item, index) {
      if (item[TEMPLATE_PREDICATE] === "http://www.w3.org/1999/02/22-rdf-syntax-ns#type") {
        obj[TEMPLATE_AT_TYPE] = item[TEMPLATE_OBJECT];
      } else if (item[TEMPLATE_IS_LITERAL]) {
        obj[Mustache.render(item[TEMPLATE_PREDICATE], jObject)] = Mustache.render(item[TEMPLATE_OBJECT], jObject);
      } else {
        obj[Mustache.render(item[TEMPLATE_PREDICATE], jObject)] = {};
        obj[Mustache.render(item[TEMPLATE_PREDICATE], jObject)][TEMPLATE_AT_ID] = Mustache.render(item[TEMPLATE_OBJECT], jObject);

      }
    });
    return obj;


    // var target = pattern[TEMPLATE_TARGET];
    // if (target) {
    //   var instanceId = Mustache.render(target, jObject);
    //   var obj = null;
    //   if (instanceId) {
    //     if (pattern["type"] === "dataProperty") {
    //       obj = instanceId;
    //     } else {
    //       obj = {};
    //       obj[TEMPLATE_AT_ID] = instanceId;
    //       if (!isProperty) {
    //         obj[TEMPLATE_AT_TYPE] = mapping;
    //       }
    //     }
    //
    //     if (properties) {
    //       properties.forEach(function(item, index) {
    //         var prop = mw._weave(item, jObject, true);
    //         if (prop)
    //           obj[item[TEMPLATE_MAPPING]] = prop;
    //       });
    //     }
    //   }
    //
    // return obj;
    // }
    // throw "Missing target from the mapping!"
  };

  function MobileWave() {
    return mw;
  };

  mw.init = function(options, callback) {
    if (options["registrationURI"]) {
      _registrationURI = options["registrationURI"];
    } else {
      throw "Registration URI is not defined!";
    };
    if (options["publicationURI"]) {
      _publicationURI = options["publicationURI"];
    } else {
      throw "Publication URI is not defined!";
    };

    if (device || options["device"]) {
      _device = device;
    } else {
      throw "Device information is undefined!"
    }

    var _register = function(template) {
      var jTemplate = (typeof template === "string" || template instanceof String) ? JSON.parse(template) : template;
      if (options["templateURI"]) {
        console.log((typeof template === "string" || template instanceof String));
        console.log(jTemplate);
      }
      _context = jTemplate["context"];
      _dGraph = jTemplate["dGraph"];
      _iGraph = jTemplate["iGraph"];
      console.log(_context);

      var dataSource = _device;
      dataSource[TEMPLATE_AT_TS] = Date.now();
      var graph = [];
      _dGraph.forEach(function(template, index) {
        graph.push(mw._weave(template, dataSource));
      });
      graph[TEMPLATE_AT_ID] = "";

      if (_registrationURI) {
        var event = {};
        event[TEMPLATE_AT_CONTEXT] = _context;
        event[TEMPLATE_AT_GRAPH] = graph;
        // TODO: Revise the data to be sent
        event[TEMPLATE_AT_TYPE] = "vois:dGraph";
        event[TEMPLATE_AT_ID] = Mustache.render("dGraph_{{uuid}}", dataSource)
        event[TEMPLATE_GENERATED] = new Date().toISOString();
        console.log("[mw]Registration URI:", _registrationURI);
        request("POST", _registrationURI, JSON.stringify(event), callback);
      }

    }

    var template = null;
    if (options["template"]) {
      _register(options["template"])
    } else if (options["templateURI"]) {
      request("GET", options["templateURI"], null, function(_template) {
        _register(_template)
      });
    } else {
      throw "No template source was provided, either define 'template' or 'templateURI'!"
    };
  };


  mw.publish = function(data, callback) {
    var dataSource = {};
    for (var key in _device) dataSource[key] = _device[key];
    for (var key in data) dataSource[key] = data[key];
    dataSource[TEMPLATE_AT_TS] = Date.now();

    var graph = [];
    _iGraph.forEach(function(template, index) {
      graph.push(mw._weave(template, dataSource));
    });
    graph[TEMPLATE_AT_ID] = "";

    var event = {};
    event[TEMPLATE_AT_CONTEXT] = _context;
    event[TEMPLATE_AT_GRAPH] = graph;
    event[TEMPLATE_AT_ID] = Mustache.render("StreamEvent_{{timestamp}}", dataSource)
    event[TEMPLATE_AT_TYPE] = "sao:StreamEvent";
    event[TEMPLATE_GENERATED] = new Date().toISOString();

    request("POST", _publicationURI, JSON.stringify(event), null);
    console.log("[mw] Publish callback!");
    console.log(data);
    console.log(dataSource);
    if (callback) {
      callback(dataSource, event);
    }

  };
  // used to add functions to MobileWave
  MobileWave.plugin = function(name, callback) {
    mw[name] = callback;
  }

  // Return a value to define the module export.
  // This example returns a functions, but the module
  // can return an object as the exported value.
  return mw;
}));
module.exports = MobileWave;
